#pragma once

#include "SFML/Graphics.hpp"
#include "Object.h"

class Projectile : public Object
{
	Vector2f m_direction;
	Vector2u m_size;
	FloatRect m_playableArea;
public:
	Projectile(sf::Color color, sf::Vector2u size, float speed, sf::Vector2f direction, TAG tag, FloatRect playableArea);

	void update(float dt);

	virtual void collision(Object* other) override;

};