#pragma once

#include "SFML/Graphics.hpp"

using namespace sf;


class Object : public sf::Drawable, public sf::Transformable
{
public:
	enum TAG
	{
		PLAYER,
		ENVIRONMENT,
		COMPUTER,
		POWERUP,
		NONE
	};
	struct Attributes
	{
		unsigned int ASlevel;
		float attackSpeed;
		unsigned int MSlevel;
		float movementSpeed;
		unsigned int lives;
	};
protected:
	VertexArray m_vertices;
	RectangleShape m_boundingBox;
	Vector2u m_size;
	bool m_isDead = false;
	TAG m_tag;
	TAG m_killedBy;

	unsigned int m_currencyValue;

	Attributes m_attributes;
public:
	Object();

	virtual void initialise();
	virtual void update(float dt) = 0;

	bool intersects(Object* obj);

	virtual void collision(Object* other);
	float getSpeed();
	Vector2u getSize();
	bool isDead();
	FloatRect getBounds();
	TAG getTag();
	TAG killedBy();

	unsigned int worth();

	void recalcBoundingBox();
protected:
	virtual void draw(RenderTarget& target, RenderStates states) const override;
};