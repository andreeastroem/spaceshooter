#pragma once
#include <vector>
#include <memory>

#include "Save.h"

using namespace std;

class SaveManager
{
	vector<Save*> m_saves;
	Save* m_current;
public:
	typedef std::unique_ptr<SaveManager> ptr;
	static ptr Create();

	SaveManager();
	~SaveManager();

	Save* getSaveFile();

	void loadFile(std::string filename);
	void saveFile();

};