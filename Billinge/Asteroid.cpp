#include "stdafx.h"
#include "Asteroid.h"

Asteroid::Asteroid(sf::Color color, FloatRect playableArea, Vector2f size, float speed, Vector2f direction, unsigned int lives)
{
	m_attributes.movementSpeed = speed;
	m_direction = direction;
	m_lives = lives;
	m_tag = TAG::ENVIRONMENT;
	m_playableArea = playableArea;

	m_vertices.setPrimitiveType(sf::Quads);
	sf::Vertex vertex = sf::Vertex(sf::Vector2f(0.f, 0.f), color);
	m_vertices.append(vertex);
	vertex = sf::Vertex(sf::Vector2f(0.f + size.x, 0.f), color);
	m_vertices.append(vertex);
	vertex = sf::Vertex(sf::Vector2f(0.f + size.x, 0.f + size.y), color);
	m_vertices.append(vertex);
	vertex = sf::Vertex(sf::Vector2f(0.f, 0.f + size.y), color);
	m_vertices.append(vertex);
}

unsigned int Asteroid::getRemainingLives()
{
	return m_lives - m_damage;
}

void Asteroid::damage()
{
	m_damage++;
	if (m_damage >= m_lives)
		m_isDead = true;
}

void Asteroid::setDirection(Vector2f direction)
{
	m_direction = direction;
}

void Asteroid::update(float dt)
{
	move(m_direction * m_attributes.movementSpeed * dt);

	if (getPosition().y + (float)m_size.y > (m_playableArea.top + m_playableArea.height))
		m_isDead = true;
}

void Asteroid::collision(Object* other)
{
	damage();
	if (isDead())
	{
		m_killedBy = other->getTag();
	}
}

void Asteroid::initialise()
{
	__super::initialise();
	
}
