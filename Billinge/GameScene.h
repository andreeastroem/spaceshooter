#pragma once

#include "Scene.h"
#include "PlayerController.h"

#include "PauseScene.h"

class GameScene : public Scene
{
	Vector2u m_shipSize;
	Vector2f m_shipStartPos;
	Color m_shipColor;
	SpaceShip* m_player;
	PlayerController* m_playerCTRL;

	Vector2u m_projectileSize;
	FloatRect m_playableArea;

	float m_asteroidSpawnDelay;
	float m_astroidTimer;

	float m_actionDelay;
	float m_actionTimer;
	PauseScene* m_pauseScreen;

public:

	virtual void update(RenderWindow* window, float dt) override;
	virtual void cleanup() override;
	virtual void initialise(int windowWidth, int windowHeight) override;


	virtual void enter() override;

};