#pragma once

#include "Scene.h"
#include "Button.h"

class UpgradeScene : public Scene
{
	std::vector<Button*> m_buttons;

	unsigned int m_currButton = 0;

	float m_actionDelay = 0.3f;
	float m_actionTimer = 0.0f;
public:
	UpgradeScene();
	~UpgradeScene();



	virtual void initialise(int windowWidth, int windowHeight) override;
	virtual void update(RenderWindow* window, float dt) override;
	virtual void cleanup() override;

};