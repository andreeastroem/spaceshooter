#include "stdafx.h"

#include "Save.h"

#include <string>
#include <algorithm>

using namespace std;

Save::Save(string directory, string filename)
{
	m_dir = directory;
	m_name = filename;
}

//TODO
bool Save::load()
{
	string fullpath = m_dir + m_name + ".save";
	string line;
	m_in.open(fullpath);

	if (m_in.is_open())
	{
		while (getline(m_in, line))
		{
			if (!interpretKey(line))
			{
				m_in.close();
				return false;
			}
		}
	}
	else
	{
		m_in.close();
		return false;
	}
	m_in.close();
	return true;
}

void Save::save(SaveData sd)
{
	string fullpath = m_dir + m_name + ".save";
	m_out.open(fullpath.c_str());

	if (m_out.is_open())
	{
		startSegment("stats");
		addKeyUIntPair("aslevel", sd.ASlevel);
		addKeyUIntPair("mslevel", sd.MSlevel);
		addKeyUIntPair("maxlives", sd.maxLives);
		addKeyUIntPair("points", sd.points);
		endSegment();
	}

	m_out.close();
}

void Save::saveSelf()
{
	save(m_sd);
}

std::string Save::getName()
{
	return m_name;
}

void Save::startSegment(string segmentName)
{
	segmentName += "\n";
	m_out << segmentName.c_str();
	m_out << "{\n";
}
void Save::addKeyUIntPair(string key, unsigned int value)
{
	string line = "\t" + key + " : " + std::to_string(value) + "\n";
	m_out << line.c_str();
}
void Save::addKeyFloatPair(std::string key, float value)
{
	string line = "\t" + key + " : " + std::to_string(value) + "\n";
	m_out << line.c_str();
}
void Save::endSegment()
{
	m_out << "}\n";
}

bool Save::interpretKey(string line)
{
	size_t delimiterpos = line.find(':');
	string key, value;

	line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
	std::transform(line.begin(), line.end(), line.begin(), ::tolower);

	if (delimiterpos != line.npos)
	{
		key = line.substr(0, delimiterpos - 1);
		value = line.substr(delimiterpos);
	}

	size_t pos = key.find("\t");
	if (pos != key.npos)
		key = key.substr(pos + 1);

	if (key == "aslevel")
	{
		m_sd.ASlevel = std::stoi(value);
	}
	else if (key == "mslevel")
	{
		m_sd.MSlevel = stoi(value);
	}
	else if (key == "maxlives")
	{
		m_sd.maxLives = stoi(value);
	}
	else if (key == "points")
	{
		m_sd.points = stoi(value);
	}

	return true;
}

unsigned int Save::getAttackSpeedLevel()
{
	return m_sd.ASlevel;
}

float Save::getAttackSpeed()
{
	return 1.f - ((float)m_sd.ASlevel * 0.1f) / (1 + 0.1f * (float)m_sd.ASlevel);
}

unsigned int Save::getMovementSpeedLevel()
{
	return m_sd.MSlevel;
}

float Save::getMovementSpeed()
{
	return 400 + m_sd.MSlevel * 20; //testa? Kanske ha �kning och base i filformat?;
}

unsigned int Save::getMaxLives()
{
	return m_sd.maxLives;
}

unsigned int Save::getPoints()
{
	return m_sd.points;
}

void Save::addPoints(unsigned int addition)
{
	m_sd.points += addition;
	points = m_sd.points;
}

void Save::incrASLevel()
{
	if (m_sd.points > cost)
	{
		m_sd.ASlevel++;
		m_sd.points -= cost;
	}
}

void Save::incrMSLevel()
{
	if (m_sd.points > cost)
	{
		m_sd.MSlevel++;
		m_sd.points -= cost;
	}
}

void Save::incrMaxLives()
{
	if (m_sd.points > cost)
	{
		m_sd.maxLives++;
		m_sd.points -= cost;
	}
}

void Save::confirm()
{
	points = m_sd.points;
}

void Save::reset()
{
	m_sd.points = points;
}

