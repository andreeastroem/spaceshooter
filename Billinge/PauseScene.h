#pragma once

#include "Scene.h"
#include "Button.h"

class PauseScene : public Scene
{
	RectangleShape* background;

	std::vector<Button*> m_buttons;

	unsigned int m_currButton = 0;

	bool m_paused = false;

	float m_actionDelay = 0.3f;
	float m_actionTimer = 0.0f;

	Scene* m_owner;
public:
	PauseScene(Scene* owner);
	virtual void update(RenderWindow* window, float dt) override;
	virtual void cleanup() override;
	virtual void initialise(int windowWidth, int windowHeight) override;

	void setPaused(bool state);
	bool isPaused();

	virtual void restart() override;

};