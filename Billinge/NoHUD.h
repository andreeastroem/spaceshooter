#pragma once

#include "HUD.h"

class NoHUD : public HUD
{

protected:
	virtual void draw(RenderTarget& target, RenderStates states) const override;

public:
	virtual void update(float dt) override;


	virtual void cleanup() override;

};