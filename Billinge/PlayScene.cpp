#include "stdafx.h"
#include "PlayScene.h"

#include "StartMenuHUD.h"

void PlayScene::initialise(int windowWidth, int windowHeight)
{
	__super::initialise(windowWidth, windowHeight);

	m_hud = new StartMenuHUD(windowWidth, windowHeight);

	//Resume button
	Button* b = new Button(windowWidth * 0.3f, windowHeight * 0.4f, Color::White, Color::Black, "RESUME");
	b->move(windowWidth * 0.15f, windowHeight * 0.5f);
	b->activate();
	auto changeSceneToGame = [&]()->void
	{
		setNext("game");
	};
	b->bind(changeSceneToGame);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);

	//Load saved button
	b = new Button(windowWidth * 0.3f, windowHeight * 0.4f, Color::White, Color::Black, "SAVES");
	b->move(windowWidth * 0.6f, windowHeight * 0.5f);
	auto changeSceneToLoadSaved = [&]()->void
	{
		setNext("loadsaved");
	};
	b->bind(changeSceneToLoadSaved);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);
}

void PlayScene::update(RenderWindow* window, float dt)
{
	__super::update(window, dt);

	m_actionTimer += dt;
	if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::X) > 30.f || Keyboard::isKeyPressed(Keyboard::Left)))
	{
		m_buttons[m_currButton]->deactivate();

		if (m_currButton == 0)
			m_currButton = m_buttons.size() - 1;
		else
			m_currButton--;

		m_buttons[m_currButton]->activate();
		m_actionTimer = 0.0f;
	}
	else if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::X) < -30.f || Keyboard::isKeyPressed(Keyboard::Right)))
	{
		m_actionTimer = 0.0f;
		m_buttons[m_currButton]->deactivate();
		m_currButton++;
		m_currButton = m_currButton % m_buttons.size();
		m_buttons[m_currButton]->activate();
	}
	else if (m_actionTimer > m_actionDelay && (Joystick::isButtonPressed(0, 0) || Keyboard::isKeyPressed(Keyboard::Enter)))
	{
		m_buttons[m_currButton]->execute();
		m_actionTimer = 0.0f;
	}
	else if (m_actionTimer > m_actionDelay && (Joystick::isButtonPressed(0, 1) || Keyboard::isKeyPressed(Keyboard::Escape)))
	{
		m_actionTimer = 0.0f;
		setNext("start");
	}

	draw(window);
}

void PlayScene::cleanup()
{
	__super::cleanup();

	for (unsigned int i = 0; i < m_buttons.size(); i++)
	{
		m_buttons[i] = nullptr;
	}
}
