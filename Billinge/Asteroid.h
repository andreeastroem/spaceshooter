#pragma once

#include "Object.h"

class Asteroid : public Object
{
	unsigned int m_lives;
	unsigned int m_damage = 0;
	Vector2f m_direction;

	FloatRect m_playableArea;
public:
	Asteroid(sf::Color color, FloatRect playableArea, Vector2f size, float speed, Vector2f direction, unsigned int lives);

	unsigned int getRemainingLives();
	void damage();
	void setDirection(Vector2f direction);

	virtual void update(float dt) override;
	virtual void collision(Object* other) override;
	virtual void initialise() override;

};