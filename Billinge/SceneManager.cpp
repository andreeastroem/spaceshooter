#include "stdafx.h"
#include "SceneManager.h"

#include "StartMenuScene.h"
#include "GameScene.h"
#include "PlayScene.h"
#include "LoadSaveScene.h"
#include "UpgradeScene.h"

#include "ServiceLocator.h"

SceneManager::ptr SceneManager::Create()
{
	return SceneManager::ptr(new SceneManager());
}

SceneManager::SceneManager()
{

}

bool SceneManager::initialise(unsigned int windowWidth, unsigned int windowHeight)
{
	m_saveManager = SaveManager::Create();
	ServiceLocator<SaveManager>::SetService(m_saveManager.get());

	Scene *scene = new StartMenuScene();
	scene->initialise(windowWidth, windowHeight);
	m_scenes.insert<std::pair<std::string, Scene*>>(std::pair<std::string, Scene*>("start", scene));
	m_currentScene = scene;

	scene = new GameScene();
	scene->initialise(windowWidth, windowHeight);
	m_scenes.insert<std::pair<std::string, Scene*>>(std::pair<std::string, Scene*>("game", scene));

	scene = new PlayScene();
	scene->initialise(windowWidth, windowHeight);
	m_scenes.insert<std::pair<std::string, Scene*>>(std::pair<std::string, Scene*>("play", scene));

	scene = new LoadSaveScene();
	scene->initialise(windowWidth, windowHeight);
	m_scenes.insert<std::pair<std::string, Scene*>>(std::pair<std::string, Scene*>("loadsaved", scene));

	scene = new UpgradeScene();
	scene->initialise(windowWidth, windowHeight);
	m_scenes.insert<std::pair<std::string, Scene* >> (std::pair<std::string, Scene*>("upgrade", scene));
	return true;
}

void SceneManager::update(RenderWindow* window, float dt)
{
	m_currentScene->update(window, dt);

	if (m_currentScene->isDone())
	{
		switchScene();
	}
}

void SceneManager::cleanup()
{
	if (m_currentScene)
		m_currentScene = nullptr;

	auto it = m_scenes.begin();
	while (it != m_scenes.end())
	{
		if (it->second)
		{
			it->second->cleanup();
			delete it->second;
			it->second = nullptr;
		}
		it++;
	}
}

void SceneManager::switchScene()
{
	std::string next = m_currentScene->getNext();
	auto it = m_scenes.find(next);
	if (it != m_scenes.end())
	{
		m_currentScene->exit();
		printf("Current scene: %s, next scene: %s\n", m_currentScene->getName().c_str(), next.c_str());
		m_currentScene = it->second;
		m_currentScene->enter();
	}
}
