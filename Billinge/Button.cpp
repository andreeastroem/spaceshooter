#include "stdafx.h"
#include "Button.h"

#include "Scene.h"
#include "ServiceLocator.h"
#include "Options.h"

void Button::initialise(Vector2f size, Color color, Color textColor, std::string text)
{
	m_font = new Font();
	m_font->loadFromFile(ServiceLocator<Options>::GetService()->fontName);
	m_buttonText = new Text(text, *m_font);
	m_buttonText->setCharacterSize(size.y * 0.18f);
	m_buttonText->setOrigin(m_buttonText->getLocalBounds().width/2, m_buttonText->getLocalBounds().height / 2);
	m_buttonText->setPosition(Vector2f(size.x / 2, size.y / 2));
	m_buttonText->setFillColor(textColor);

	m_box = new RectangleShape(size);
	m_box->setFillColor(color);
	m_overlay = new RectangleShape(size);
	m_overlay->setFillColor(Color(0, 0, 0, 124));
}

Button::Button()
{
	initialise(Vector2f(), Color::Black, Color::White, "");
}

Button::Button(Vector2f size, Color color, Color textColor, std::string text /*= ""*/)
{
	initialise(size, color, textColor, text);
}

Button::Button(float x, float y, Color color, Color textColor, std::string text /*= ""*/)
{
	initialise(Vector2f(x, y), color, textColor, text);
}

Button::~Button()
{
	if (m_box)
	{
		delete m_box;
		m_box = nullptr;
	}
	if (m_overlay)
	{
		delete m_overlay;
		m_overlay = nullptr;
	}
	if (m_buttonText)
	{
		delete m_buttonText;
		m_buttonText = nullptr;
	}
	if (m_font)
	{
		delete m_font;
		m_font = nullptr;
	}
}

void Button::update(float dt)
{

}

void Button::setText(std::string text)
{
	m_buttonText->setString(text);

	//Text resizing
}

std::string Button::getText()
{
	return m_buttonText->getString();
}

void Button::setCharSize(unsigned int charsize)
{
	m_buttonText->setCharacterSize(charsize);
	m_buttonText->setOrigin(m_buttonText->getLocalBounds().width / 2, 
		m_buttonText->getLocalBounds().height / 2);
}

void Button::setSize(Vector2f size)
{
	m_box->setSize(size);
	m_overlay->setSize(size);

	//Text resizing
}

void Button::setSize(float x, float y)
{
	setSize(Vector2f(x, y));
}

void Button::setColour(Color color)
{
	m_box->setFillColor(color);
}

void Button::activate()
{
	m_active = true;
}

void Button::deactivate()
{
	m_active = false;
}

void Button::bind(std::function<void()> func)
{
	m_func = func;
	m_bound = true;
}

void Button::unbind()
{
	m_bound = false;
}

void Button::execute()
{
	//if (m_owner)
	//	m_owner->setNext(m_next);

	if(m_bound)
		m_func();
}

void Button::draw(RenderTarget& target, RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(*m_box, states);
	target.draw(*m_buttonText, states);
	if (!m_active)
		target.draw(*m_overlay, states);
}
