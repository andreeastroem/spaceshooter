#include "stdafx.h"
#include "PlayerController.h"

#include "SaveManager.h"
#include "ServiceLocator.h"

PlayerController::PlayerController(SpaceShip* player, Vector2u shipSize, ObjectManager* objMgr, FloatRect playableArea)
{
	m_player = player;
	m_objectManager = objMgr;
	m_playableArea = playableArea;

	m_shipSize = shipSize;
	
	m_projectileSize = Vector2u(4, 4);
}

void PlayerController::update(float dt)
{
	if (m_gamepadEnabled)
		gamepadControl(dt);
	else
		keyboardControl(dt);
}

void PlayerController::cleanup()
{

}

void PlayerController::gamepadControl(float dt)
{
	//Horizontal
	float x = Joystick::getAxisPosition(0, Joystick::X);

	Vector2f movement;
	movement.x = dt * (x / 100) * m_player->getSpeed();
	movement.y = 0;

	//Vertical
	float y = Joystick::getAxisPosition(0, Joystick::Y);
	movement.y = dt * (y / 100) * m_player->getSpeed();

	//deadzone
	if (abs(x) > 30)
	{
		float targetX = 0.f;
		if (x < 0)
			targetX = m_player->getPosition().x + movement.x;
		else if (x > 0)
			targetX = m_player->getPosition().x + movement.x + m_shipSize.x;
		if (m_playableArea.contains(Vector2f(targetX, m_player->getPosition().y)))
			m_player->move(movement.x, 0);
	}
	if (abs(y) > 30)
	{
		float targetY = 0.f;
		if (y < 0)
			targetY = m_player->getPosition().y + movement.y;
		if (y > 0)
			targetY = m_player->getPosition().y + movement.y + m_shipSize.y;

		if (m_playableArea.contains(Vector2f(m_player->getPosition().x, targetY)))
			m_player->move(0, movement.y);
	}

	//Instantiating bullets
	if (Joystick::isButtonPressed(0, 0) && m_player->canFire())
	{
		Projectile* p = new Projectile(sf::Color::Red, m_projectileSize, m_projectileSpeed, sf::Vector2f(0, -1), m_player->getTag(), m_playableArea);
		p->move(m_player->getPosition().x + m_shipSize.x / 3, m_player->getPosition().y - m_projectileSize.y - 1);

		m_objectManager->attachObject(p);

		p = new Projectile(sf::Color::Red, m_projectileSize, m_projectileSpeed, sf::Vector2f(0, -1), m_player->getTag(), m_playableArea);
		p->move(m_player->getPosition().x + (2 * m_shipSize.x) / 3, m_player->getPosition().y - m_projectileSize.y - 1);

		m_objectManager->attachObject(p);

		m_player->fire();
	}
}

void PlayerController::keyboardControl(float dt)
{
	//Horizontal
	float x = 0;
	if (Keyboard::isKeyPressed(Keyboard::Left))
		x = -100;
	else if (Keyboard::isKeyPressed(Keyboard::Right))
		x = 100;

	Vector2f movement;
	movement.x = dt * (x / 100) * m_player->getSpeed();
	movement.y = 0;

	//Vertical
	float y = 0;
	if (Keyboard::isKeyPressed(Keyboard::Up))
		y = -100;
	else if (Keyboard::isKeyPressed(Keyboard::Down))
		y = 100;

	movement.y = dt * (y / 100) * m_player->getSpeed();

	//deadzone
	if (abs(x) > 30)
	{
		float targetX = 0.f;
		if (x < 0)
			targetX = m_player->getPosition().x + movement.x;
		else if (x > 0)
			targetX = m_player->getPosition().x + movement.x + m_shipSize.x;
		if (m_playableArea.contains(Vector2f(targetX, m_player->getPosition().y)))
			m_player->move(movement.x, 0);
	}
	if (abs(y) > 30)
	{
		float targetY = 0.f;
		if (y < 0)
			targetY = m_player->getPosition().y + movement.y;
		if (y > 0)
			targetY = m_player->getPosition().y + movement.y + m_shipSize.y;

		if (m_playableArea.contains(Vector2f(m_player->getPosition().x, targetY)))
			m_player->move(0, movement.y);
	}

	//Instantiating bullets
	if (Keyboard::isKeyPressed(Keyboard::Space) && m_player->canFire())
	{
		Projectile* p = new Projectile(sf::Color::Red, m_projectileSize, m_projectileSpeed, sf::Vector2f(0, -1), m_player->getTag(), m_playableArea);
		p->move(m_player->getPosition().x + m_shipSize.x / 3, m_player->getPosition().y - m_projectileSize.y - 1);

		m_objectManager->attachObject(p);

		p = new Projectile(sf::Color::Red, m_projectileSize, m_projectileSpeed, sf::Vector2f(0, -1), m_player->getTag(), m_playableArea);
		p->move(m_player->getPosition().x + (2 * m_shipSize.x) / 3, m_player->getPosition().y - m_projectileSize.y - 1);

		m_objectManager->attachObject(p);

		m_player->fire();
	}
}
