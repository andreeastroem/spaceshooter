#include "stdafx.h"
#include "Projectile.h"

Projectile::Projectile(sf::Color color, sf::Vector2u size, float speed, sf::Vector2f direction, TAG tag, FloatRect playableArea)
{
	m_attributes.movementSpeed = speed;
	m_direction = direction;
	m_tag = tag;
	m_size = size;
	m_playableArea = playableArea;

	m_vertices.setPrimitiveType(sf::Quads);
	sf::Vertex vertex = sf::Vertex(sf::Vector2f(0.f, 0.f), color);
	m_vertices.append(vertex);
	vertex = sf::Vertex(sf::Vector2f(0.f + size.x, 0.f), color);
	m_vertices.append(vertex);
	vertex = sf::Vertex(sf::Vector2f(0.f + size.x, 0.f + size.y), color);
	m_vertices.append(vertex);
	vertex = sf::Vertex(sf::Vector2f(0.f, 0.f + size.y), color);
	m_vertices.append(vertex);
}

void Projectile::update(float dt)
{
	move(m_direction * m_attributes.movementSpeed * dt);
	
	if (!m_playableArea.contains(getPosition()) 
		|| !m_playableArea.contains(getPosition().x, getPosition().y + (float)m_size.y))
	{
		m_isDead = true;
	}
}

void Projectile::collision(Object* other)
{
	if (m_tag != other->getTag())
	{
		m_isDead = true;
	}
}
