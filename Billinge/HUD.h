#pragma once

#include "SFML/Graphics.hpp"

#include <vector>

using namespace sf;

class HUD : public sf::Transformable, public sf::Drawable
{
protected:

	virtual void draw(RenderTarget& target, RenderStates states) const = 0;

public:
	HUD();
	virtual void update(float dt);
	virtual void cleanup();

protected:
};