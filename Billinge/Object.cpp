#include "stdafx.h"
#include "Object.h"


Object::Object()
{

}

void Object::initialise()
{
	m_currencyValue = 10;
	m_killedBy = NONE;
	recalcBoundingBox();
}

void Object::update(float dt)
{

}

bool Object::intersects(Object* obj)
{
	return getBounds().intersects(obj->getBounds());
}

void Object::collision(Object* other)
{

}

float Object::getSpeed()
{
	return m_attributes.movementSpeed;
}

sf::Vector2u Object::getSize()
{
	return m_size;
}

bool Object::isDead()
{
	return m_isDead;
}

sf::FloatRect Object::getBounds()
{
	return getTransform().transformRect(m_vertices.getBounds());
}

Object::TAG Object::getTag()
{
	return m_tag;
}

Object::TAG Object::killedBy()
{
	return m_killedBy;
}

unsigned int Object::worth()
{
	return m_currencyValue;
}

void Object::recalcBoundingBox()
{
	m_boundingBox = RectangleShape(Vector2f(m_vertices.getBounds().width, m_vertices.getBounds().height));
	m_boundingBox.setFillColor(Color::Yellow);
	m_boundingBox.setFillColor(Color(m_boundingBox.getFillColor().r, m_boundingBox.getFillColor().g, m_boundingBox.getFillColor().b, 120));
	m_boundingBox.setOutlineThickness(1.f);
	m_boundingBox.setOutlineColor(Color::Yellow);
}

void Object::draw(RenderTarget& target, RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(m_boundingBox, states);
	//target.draw(m_vertices, states);
}