#pragma once

#include "Scene.h"
#include "SaveManager.h"

#include <memory>
#include <map>


class SceneManager
{
	SaveManager::ptr m_saveManager;

public:
	typedef std::unique_ptr<SceneManager> ptr;
	static ptr Create();

	SceneManager();
	
	bool initialise(unsigned int windowWidth, unsigned int windowHeight);
	void update(RenderWindow* window, float dt);
	void cleanup();

	void switchScene();
private:
	std::map<std::string, Scene*> m_scenes;
	Scene* m_currentScene;
};