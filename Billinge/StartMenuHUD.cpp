#include "stdafx.h"
#include "StartMenuHUD.h"

#include "ServiceLocator.h"
#include "Options.h"

StartMenuHUD::StartMenuHUD(unsigned int windowWidth, unsigned int windowHeight)
{
	m_font = new Font();
	m_font->loadFromFile(ServiceLocator<Options>::GetService()->fontName);
	m_logo = new Text();
	m_logo->setFont(*m_font);
	m_logo->setCharacterSize((float)windowHeight * 0.15f);
	m_logo->setString("Spaceshooter");
	m_logo->setOrigin(m_logo->getLocalBounds().width / 2, m_logo->getLocalBounds().height / 2);
	m_logo->setPosition((float)windowWidth * 0.5f, (float)windowHeight * 0.2f);
	m_logo->setFillColor(Color::White);
}

void StartMenuHUD::draw(RenderTarget& target, RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(*m_logo, states);
}
