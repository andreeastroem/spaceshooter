#include "stdafx.h"
#include "StartMenuScene.h"

#include "StartMenuHUD.h"

#include "ServiceLocator.h"
#include "SceneManager.h"

void StartMenuScene::update(RenderWindow* window, float dt)
{
	__super::update(window, dt);
	
	m_actionTimer += dt;
	if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::X) > 30.f || Keyboard::isKeyPressed(Keyboard::Left)))
	{
		m_buttons[m_currButton]->deactivate();

		if (m_currButton == 0)
			m_currButton = m_buttons.size() - 1;
		else
			m_currButton--;

		m_buttons[m_currButton]->activate();
		m_actionTimer = 0.0f;
	}
	if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::X) < -30.f || Keyboard::isKeyPressed(Keyboard::Right)))
	{
		m_actionTimer = 0.0f;
		m_buttons[m_currButton]->deactivate();
		m_currButton++;
		m_currButton = m_currButton % m_buttons.size();
		m_buttons[m_currButton]->activate();
	}

	if (m_actionTimer > m_actionDelay && (Joystick::isButtonPressed(0, 0) || Keyboard::isKeyPressed(Keyboard::Enter)))
	{
		m_buttons[m_currButton]->execute();
		m_actionTimer = 0.0f;
	}

	draw(window);
}

void StartMenuScene::cleanup()
{
	__super::cleanup();
	
	for (unsigned int i = 0; i < m_buttons.size(); i++)
	{
		m_buttons[i] = nullptr;
	}
}


void StartMenuScene::initialise(int windowWidth, int windowHeight)
{
	__super::initialise(windowWidth, windowHeight);

	m_hud = new StartMenuHUD(windowWidth, windowHeight);

	//Play button
	Button* b = new Button(windowWidth * 0.3f, windowHeight * 0.4f, Color::White, Color::Black, "PLAY");
	b->move(windowWidth * 0.15f, windowHeight * 0.5f);
	b->activate();
	auto changeSceneToPlay = [&]()->void 
	{
		setNext("play");
	};
	b->bind(changeSceneToPlay);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);

	

	//Upgrade button
	b = new Button(windowWidth * 0.3f, windowHeight * 0.4f, Color::White, Color::Black, "UPGRADES");
	b->move(windowWidth * 0.6f, windowHeight * 0.5f);
	auto changeSceneToUpgrade = [&]()->void
	{
		setNext("upgrade");
	};
	b->bind(changeSceneToUpgrade);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);

	//memb variable initialisation
	m_currButton = 0;
	m_actionDelay = 0.3f;
	m_actionTimer = 0.0f;
	m_name = "start";
}

