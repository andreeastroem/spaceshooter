#pragma once

#include "Scene.h"
#include "Button.h"

class LoadSaveScene : public Scene
{
	std::vector<Button*> m_buttons;

	unsigned int m_currButton = 0;

	bool m_paused = false;

	float m_actionDelay = 0.3f;
	float m_actionTimer = 0.0f;

public:
	virtual void initialise(int windowWidth, int windowHeight) override;
	virtual void update(RenderWindow* window, float dt) override;
	virtual void cleanup() override;

};