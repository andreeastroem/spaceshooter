#pragma once

#include <iostream>
#include <fstream>

struct SaveData
{
	unsigned int ASlevel;
	float attackSpeed;
	unsigned int MSlevel;
	float movementSpeed;
	unsigned int maxLives;
	unsigned int points;
};

class Save
{
	std::string m_dir, m_name;

	std::ofstream m_out;
	std::ifstream m_in;

	SaveData m_sd;

	unsigned int points;
	unsigned int cost = 10;

public:
	Save(std::string directory, std::string filename);
	
	bool load();
	void save(SaveData sd);
	void saveSelf();

	std::string getName();

private:
	void startSegment(std::string segmentName);
	void addKeyUIntPair(std::string key, unsigned int value);
	void addKeyFloatPair(std::string key, float value);
	void endSegment();

	bool interpretKey(std::string line);

public:
	unsigned int getAttackSpeedLevel();
	float getAttackSpeed();
	unsigned int getMovementSpeedLevel();
	float getMovementSpeed();
	unsigned int getMaxLives();
	unsigned int getPoints();

	void addPoints(unsigned int addition);
	void incrASLevel();
	void incrMSLevel();
	void incrMaxLives();

	void confirm();
	void reset();
private:
	
};