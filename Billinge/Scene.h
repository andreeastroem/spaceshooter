#pragma once

#include "SFML/Graphics.hpp"
#include "Object.h"

#include "ObjectManager.h"
#include "HUD.h"

class Scene
{
protected:
	int m_windowWidth, m_windowHeight;

	ObjectManager* m_objectManager;
	HUD* m_hud;

	bool m_done = false;
	std::string m_nextScene = "none";
	std::string m_name = "none";

	void draw(RenderWindow* window);

public:
	Scene();

	virtual void initialise(int windowWidth, int windowHeight);
	virtual void update(RenderWindow* window, float dt) = 0;
	virtual void restart();
	virtual void cleanup() = 0;
	virtual void enter();

	bool isDone();
	std::string getNext();
	std::string getName();

	void setNext(std::string next);
	void exit();
};