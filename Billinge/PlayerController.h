#pragma once

#include "SpaceShip.h"
#include "Projectile.h"
#include "ObjectManager.h"



class PlayerController
{
	SpaceShip* m_player;
	ObjectManager* m_objectManager;

	FloatRect m_playableArea;

	Vector2u m_shipSize;
	Vector2f m_shipStartPos;
	Color m_shipColor;

	Vector2u m_projectileSize;
	float m_projectileSpeed = 1200.f;

	bool m_gamepadEnabled = false;
public:
	PlayerController(SpaceShip* player, Vector2u shipSize, ObjectManager* objMgr, FloatRect playableArea);


	void update(float dt);
	void cleanup();

private:
	void gamepadControl(float dt);
	void keyboardControl(float dt);
};