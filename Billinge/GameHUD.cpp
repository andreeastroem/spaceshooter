#include "stdafx.h"
#include "GameHUD.h"

#include "ServiceLocator.h"
#include "SaveManager.h"

#include "Options.h"

GameHUD::GameHUD(SpaceShip* player, FloatRect playableArea, unsigned int width, unsigned int height, unsigned int maxHealth)
{
	Color hudColor = Color(0, 0, 255, 122);

	m_player = player;
	m_playableArea = playableArea;

	m_HUDtop.left = 0.f;
	m_HUDtop.top = 0.f;
	m_HUDtop.width = (float)width;
	m_HUDtop.height = m_playableArea.top;

	m_topbar = new RectangleShape(Vector2f(m_HUDtop.width, m_HUDtop.height));
	m_topbar->move(m_HUDtop.left, m_HUDtop.top);
	m_topbar->setFillColor(hudColor);

	m_HUDbottom.left = 0.f;
	m_HUDbottom.top = m_playableArea.top + m_playableArea.height;
	m_HUDbottom.width = (float)width;
	m_HUDbottom.height = height - m_HUDbottom.top;

	m_bottombar = new RectangleShape(Vector2f(m_HUDbottom.width, m_HUDbottom.height));
	m_bottombar->move(m_HUDbottom.left, m_HUDbottom.top);
	m_bottombar->setFillColor(hudColor);

	unsigned int columns = 20;
	unsigned int border = 2;
	unsigned int nrOfRows = 2;
	float rowBorder = m_HUDtop.height * 0.1f;

	float lifeIndicatorWidth = (((m_HUDtop.width / 2) - (border * columns)) * (1.0f / columns));
	float lifeIndicatorHeight = (m_HUDtop.height - ((nrOfRows + 1) * rowBorder)) / nrOfRows;;
	RectangleShape* HUDLifeIndicator;
	int xpos = (int)m_HUDtop.left;
	for (unsigned int i = 0; i < maxHealth; i++)
	{
		xpos += border;
		HUDLifeIndicator = new RectangleShape(Vector2f(lifeIndicatorWidth, lifeIndicatorHeight));
		HUDLifeIndicator->move(xpos,
			m_HUDtop.top + (int)lifeIndicatorHeight * (i / columns) + (int)rowBorder * ((i / columns) + 1));

		HUDLifeIndicator->setFillColor(Color::Green);
		m_lives.push_back(HUDLifeIndicator);

		xpos += (int)lifeIndicatorWidth;
		xpos = xpos % (width / 2);
	}

	m_font = new Font();
	m_font->loadFromFile(ServiceLocator<Options>::GetService()->fontName);
	m_logo = new Text();
	m_logo->setFont(*m_font);
	m_logo->setString("Spaceshooter");
	m_logo->setPosition(m_HUDbottom.left + m_HUDbottom.width / 4.f, m_HUDbottom.top + m_HUDbottom.height * 0.1f);
	m_logo->setCharacterSize(m_HUDbottom.height * 0.8f);
	m_logo->setFillColor(Color::White);

	m_currTextVMargin = 0.3f;
	m_currTextHMargin = 0.02f;

	m_currencyText = new Text();
	m_currencyText->setFont(*m_font);
	m_currencyText->setString("Currency: ");
	m_currencyText->setPosition(m_HUDtop.left + (m_HUDtop.width / 2 + m_HUDtop.width*m_currTextHMargin),
		m_HUDtop.top + m_HUDtop.height * m_currTextVMargin);
	m_currencyText->setCharacterSize(m_HUDtop.height *  (1 - (m_currTextVMargin *2)));
	m_currencyText->setFillColor(Color::White);

	m_currency = new Text();
	m_currency->setFont(*m_font);
	m_currency->setString("0");
	m_currency->setOrigin(m_currency->getLocalBounds().width, m_currency->getLocalBounds().height);
	m_currency->setPosition(m_HUDtop.width - (m_HUDtop.width * m_currTextHMargin),
		m_HUDtop.top + m_HUDtop.height * m_currTextVMargin);
	m_currency->setCharacterSize(m_HUDtop.height * ( 1- (m_currTextVMargin *2)));
	m_currency->setFillColor(Color::White);

	updateSaveInstance();
}

void GameHUD::update(float dt)
{
	updateCurrency();
}

void GameHUD::cleanup()
{
	for (unsigned int i = 0; i < m_lives.size(); i++)
	{
		if (m_lives[i])
		{
			delete m_lives[i];
			m_lives[i] = nullptr;
		}
	}

	if (m_bottombar)
	{
		delete m_bottombar;
		m_bottombar = nullptr;
	}
	if (m_topbar)
	{
		delete m_topbar;
		m_topbar = nullptr;
	}

	if (m_logo)
	{
		delete m_logo;
		m_logo = nullptr;
	}
	if (m_font)
	{
		delete m_font;
		m_font = nullptr;
	}

	if (m_player)
		m_player = nullptr;
}

void GameHUD::updateCurrency()
{
	m_currency->setString(std::to_string(m_save->getPoints()));
	m_currency->setOrigin(m_currency->getLocalBounds().width, m_currency->getLocalBounds().height);
	m_currency->setPosition(m_HUDtop.width - (m_HUDtop.width * m_currTextHMargin),
		m_HUDtop.top + m_HUDtop.height * m_currTextVMargin + m_currency->getLocalBounds().height);

	Vector2f pos = m_currency->getPosition();
	Vector2f orig = m_currency->getOrigin();
}

void GameHUD::updateSaveInstance()
{
	m_save = ServiceLocator<SaveManager>::GetService()->getSaveFile();
}

void GameHUD::draw(RenderTarget& target, RenderStates states) const
{
	states.transform *= getTransform();

	target.draw(*m_bottombar, states);
	target.draw(*m_topbar, states);

	if (m_player->getRemainingLives() % nrOfColours == 1)
		m_lives[(m_player->getRemainingLives() / nrOfColours)]->setFillColor(Color::Yellow);

	for (unsigned int i = 0; i < (m_player->getRemainingLives() / nrOfColours + m_player->getRemainingLives() % nrOfColours); i++)
	{
		target.draw(*m_lives[i], states);
	}
	target.draw(*m_logo, states);
	target.draw(*m_currencyText, states);
	target.draw(*m_currency, states);
}