#include "stdafx.h"

#include "LoadSaveScene.h"
#include "NoHUD.h"

#include "SaveManager.h"
#include "ServiceLocator.h"

void LoadSaveScene::initialise(int windowWidth, int windowHeight)
{
	__super::initialise(windowWidth, windowHeight);

	m_hud = new NoHUD();

	//Slot 01
	Button* b = new Button(windowWidth * 0.38f, windowHeight * 0.2f, Color::White, Color::Black, "SAVE 01");
	b->move(windowWidth * 0.6f, windowHeight * 0.2f);
	b->activate();
	auto loadAndStart01 = [&]()->void
	{
		ServiceLocator<SaveManager>::GetService()->loadFile("save01");
		setNext("game");
	};
	b->bind(loadAndStart01);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);

	//Slot 02
	b = new Button(windowWidth * 0.38f, windowHeight * 0.2f, Color::White, Color::Black, "SAVE 02");
	b->move(windowWidth * 0.6f, windowHeight * 0.45f);
	auto loadAndStart02 = [&]()->void
	{
		ServiceLocator<SaveManager>::GetService()->loadFile("save02");
		setNext("game");
	};
	b->bind(loadAndStart02);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);

	//Slot 03
	b = new Button(windowWidth * 0.38f, windowHeight * 0.2f, Color::White, Color::Black, "SAVE 03");
	b->move(windowWidth * 0.6f, windowHeight * 0.70f);
	auto loadAndStart03 = [&]()->void
	{
		ServiceLocator<SaveManager>::GetService()->loadFile("save03");
		setNext("game");
	};
	b->bind(loadAndStart03);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);
}

void LoadSaveScene::update(RenderWindow* window, float dt)
{
	__super::update(window, dt);

	m_actionTimer += dt;
	if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::Y) > 30.f || Keyboard::isKeyPressed(Keyboard::Up)))
	{
		m_buttons[m_currButton]->deactivate();

		if (m_currButton == 0)
			m_currButton = m_buttons.size() - 1;
		else
			m_currButton--;

		m_buttons[m_currButton]->activate();
		m_actionTimer = 0.0f;
	}
	else if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::Y) < -30.f || Keyboard::isKeyPressed(Keyboard::Down)))
	{
		m_actionTimer = 0.0f;
		m_buttons[m_currButton]->deactivate();
		m_currButton++;
		m_currButton = m_currButton % m_buttons.size();
		m_buttons[m_currButton]->activate();
	}
	else if (m_actionTimer > m_actionDelay && (Joystick::isButtonPressed(0, 0) || Keyboard::isKeyPressed(Keyboard::Enter)))
	{
		m_buttons[m_currButton]->execute();
		m_actionTimer = 0.0f;
	}
	else if (m_actionTimer > m_actionDelay && (Joystick::isButtonPressed(0, 1) || Keyboard::isKeyPressed(Keyboard::Escape)))
	{
		m_actionTimer = 0.0f;
		setNext("start");
	}

	draw(window);
}

void LoadSaveScene::cleanup()
{
	
}
