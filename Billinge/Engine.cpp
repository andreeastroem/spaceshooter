#include "stdafx.h"
#include "Engine.h"
#include "Projectile.h"
#include "Asteroid.h"

#include "GameScene.h"
#include "StartMenuScene.h"

#include "ServiceLocator.h"

//Test

bool Engine::initialise(int windowWidth, int windowHeight)
{
	srand((unsigned int)time(NULL));
	
	m_options = Options::Create();
	ServiceLocator<Options>::SetService(m_options.get());

	m_windowWidth = windowWidth;
	m_windowHeight = windowHeight;

	ContextSettings settings;
	settings.antialiasingLevel = 8;

	m_window = new RenderWindow(VideoMode(windowWidth, windowHeight), "Space shooter", Style::Titlebar | Style::Close, settings);
	m_window->setJoystickThreshold(m_joystickDeadzone);
	

	float dt = 0.f;
	m_currency = 0;
	
	

	m_sceneMgr = SceneManager::Create();
	m_sceneMgr->initialise(windowWidth, windowHeight);

	ServiceLocator<SceneManager>::SetService(m_sceneMgr.get());

	

	return true;
}

void Engine::run()
{
	while (m_window->isOpen())
	{
		dt = m_clock.restart().asSeconds();
		Event e;
		while (m_window->pollEvent(e))
		{
			switch (e.type)
			{
			case sf::Event::Closed:
				m_window->close();
				break;
			case sf::Event::KeyPressed:
				//if (e.key.code == Keyboard::Escape)
				//	m_window->close();
				break;
			case sf::Event::JoystickButtonPressed:
				//printf("BUTTON ID: %i\n", e.joystickButton.button);
				break;
			case sf::Event::JoystickMoved:
				//printf("JoystickID: %i, Axis ID: %i with value: %f\n", e.joystickMove.joystickId, e.joystickMove.axis, e.joystickMove.position);
				break;
			}
		}

		m_sceneMgr->update(m_window, dt);
	}
}

void Engine::cleanup()
{
	if (m_window)
	{
		delete m_window;
		m_window = nullptr;
	}

	if (m_player)
	{
		delete m_player;
		m_player = nullptr;
	}
}

