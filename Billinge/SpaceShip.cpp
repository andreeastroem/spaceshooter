#include "stdafx.h"
#include "SpaceShip.h"

SpaceShip::SpaceShip(TAG tag)
{
	m_tag = tag;

	sf::Vertex vertex = sf::Vertex(sf::Vector2f(0, 0), sf::Color::Blue);
	m_vertices.append(vertex);
	vertex = sf::Vertex(sf::Vector2f(1, 0), sf::Color::Blue);
	m_vertices.append(vertex);
	vertex = sf::Vertex(sf::Vector2f(1, 1), sf::Color::Blue);
	m_vertices.append(vertex);
	vertex = sf::Vertex(sf::Vector2f(0, 1), sf::Color::Blue);
	m_vertices.append(vertex);

	m_attributes.attackSpeed = 0.5f;
	m_fireTimer = 0.f;
	m_attributes.lives = 1;
	m_damage = 0;
}

void SpaceShip::update(float dt)
{
	m_fireTimer += dt;
}

bool SpaceShip::canFire()
{
	return m_fireTimer > m_attributes.attackSpeed;
}

void SpaceShip::fire()
{
	m_fireTimer = 0.f;
}

void SpaceShip::damage()
{
	m_damage++;
	if (m_damage >= m_attributes.lives)
	{
		m_damage = m_attributes.lives;
		m_isDead = true;
	}
}

void SpaceShip::heal(int healthToHeal)
{
	if (healthToHeal > m_damage)
		m_damage = 0;
	else
		m_damage - healthToHeal;
}

unsigned int SpaceShip::getRemainingLives()
{
	return m_attributes.lives - m_damage;
}

void SpaceShip::setSize(sf::Vector2u size)
{
	m_vertices[1].position = sf::Vector2f(0.f + size.x, 0.f);
	m_vertices[2].position = sf::Vector2f(0.f + size.x, 0.f + size.y);
	m_vertices[3].position = sf::Vector2f(0.f, 0.f + size.y);
	recalcBoundingBox();
}

void SpaceShip::setColour(sf::Color color)
{
	for(int i = 0; i < m_vertices.getVertexCount(); i++)
	{
		m_vertices[i].color = color;
	}
}

void SpaceShip::setSpeed(float speed)
{
	m_attributes.movementSpeed = speed;
}

void SpaceShip::setAttackSpeed(float speed)
{
	m_attributes.attackSpeed = speed;
}

void SpaceShip::setMaxLives(unsigned int lives)
{
	if(lives > 0)
		m_attributes.lives = lives;
}

void SpaceShip::collision(Object* other)
{
	damage();
}
