#include "stdafx.h"
#include "ObjectManager.h"

#include "ServiceLocator.h"
#include "SaveManager.h"

void ObjectManager::update(float dt)
{
	for each (Object* o in m_objects)
	{
		o->update(dt);
	}

	for (unsigned int i = 0; (i + 1) < m_objects.size(); i++)
	{
		for (unsigned int j = i + 1; j < m_objects.size(); j++)
		{
			if (m_objects[i]->intersects(m_objects[j]))
			{
				m_objects[i]->collision(m_objects[j]);
				m_objects[j]->collision(m_objects[i]);
			}
		}
	}

	for (int i = m_objects.size() - 1; i >= 0; i--)
	{
		if (m_objects[i]->isDead())
		{
			if (m_objects[i]->killedBy() == Object::PLAYER)
				m_value += m_objects[i]->worth();
			m_objects.erase(m_objects.begin() + i);
		}
	}

	ServiceLocator<SaveManager>::GetService()->getSaveFile()->addPoints(m_value);
	m_value = 0;
}

/************************************************************************/
/*					Initialises object and attaches it					*/
/************************************************************************/
void ObjectManager::attachObject(Object* object)
{
	object->initialise();
	m_objects.push_back(object);
}

std::vector<Object*> ObjectManager::getObjects()
{
	return m_objects;
}
