#pragma once

#include <vector>

#include "SpaceShip.h"
#include "ObjectManager.h"
#include "HUD.h"
#include "PlayerController.h"
#include "Scene.h"

#include "SceneManager.h"
#include "Options.h"

using namespace sf;

class Engine
{
	int m_windowWidth, m_windowHeight;
	RenderWindow* m_window;
	float m_joystickDeadzone = 30.f;

	SpaceShip* m_player;

	FloatRect m_playableArea;

	Clock m_clock;

	Scene* m_scene;

	SceneManager::ptr m_sceneMgr;
	Options::ptr m_options;

	unsigned int m_currency;
public:
	float dt = 0.f;

	bool initialise(int windowWidth, int windowHeight);
	void run();
	void cleanup();
};