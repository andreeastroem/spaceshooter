#include "stdafx.h"

#include "UpgradeScene.h"

#include "SaveManager.h"
#include "ServiceLocator.h"
#include "StartMenuHUD.h"

UpgradeScene::UpgradeScene()
{

}

UpgradeScene::~UpgradeScene()
{

}

void UpgradeScene::initialise(int windowWidth, int windowHeight)
{
	__super::initialise(windowWidth, windowHeight);

	m_hud = new StartMenuHUD(windowWidth, windowHeight);

	//Maxlives Increment
	Button* b = new Button(windowWidth * 0.1f, windowHeight * 0.15f, Color::White, Color::Black, "+");
	b->move(windowWidth * 0.15f, windowHeight * 0.3f);
	b->activate();
	b->setCharSize(50); 
	auto maxLivesIncrement = [&]()->void
	{
		ServiceLocator<SaveManager>::GetService()->getSaveFile()->incrMaxLives();
	};
	b->bind(maxLivesIncrement);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);

	//Maxlives decrement (if points been allocated)
	b = new Button(windowWidth * 0.1f, windowHeight * 0.15f, Color::White, Color::Black, "-");
	b->move(windowWidth * 0.15f, windowHeight * 0.45f);
	b->setCharSize(50);
	auto maxlivesDecrement = [&]()->void
	{
		
	};
	b->bind(maxlivesDecrement);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);
}

void UpgradeScene::update(RenderWindow* window, float dt)
{
	__super::update(window, dt);

	m_actionTimer += dt;
	if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::X) > 30.f || Keyboard::isKeyPressed(Keyboard::Left)))
	{
		m_buttons[m_currButton]->deactivate();

		if (m_currButton == 0)
			m_currButton = m_buttons.size() - 1;
		else
			m_currButton--;

		m_buttons[m_currButton]->activate();
		m_actionTimer = 0.0f;
	}
	else if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::X) < -30.f || Keyboard::isKeyPressed(Keyboard::Right)))
	{
		m_actionTimer = 0.0f;
		m_buttons[m_currButton]->deactivate();
		m_currButton++;
		m_currButton = m_currButton % m_buttons.size();
		m_buttons[m_currButton]->activate();
	}
	else if (m_actionTimer > m_actionDelay && (Joystick::isButtonPressed(0, 0) || Keyboard::isKeyPressed(Keyboard::Enter)))
	{
		m_buttons[m_currButton]->execute();
		m_actionTimer = 0.0f;
		printf("executed button nr %i\n", m_currButton);
	}
	else if (m_actionTimer > m_actionDelay && (Joystick::isButtonPressed(0, 1) || Keyboard::isKeyPressed(Keyboard::Escape)))
	{
		m_actionTimer = 0.0f;
		setNext("start");
	}

	draw(window);
}

void UpgradeScene::cleanup()
{
	__super::cleanup();
	for (unsigned int i = 0; i < m_buttons.size(); i++)
	{
		delete m_buttons[i];
		m_buttons[i] = nullptr;
	}
}
