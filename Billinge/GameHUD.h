#pragma once

#include "SFML/Graphics.hpp"
#include "HUD.h"
#include "SpaceShip.h"
#include "Save.h"

class GameHUD : public HUD
{
	Save* m_save;
	SpaceShip* m_player;
	FloatRect m_playableArea;

	std::vector<RectangleShape*> m_lives;

	FloatRect m_HUDtop, m_HUDbottom;
	RectangleShape* m_bottombar, *m_topbar;

	unsigned int nrOfColours = 2;

	Font* m_font;
	Text* m_logo;

	Text* m_currency;
	Text* m_currencyText;

	float m_currTextVMargin;
	float m_currTextHMargin;
public:
	GameHUD(SpaceShip* player, FloatRect playableArea, unsigned int width, unsigned int height, unsigned int maxHealth);

	virtual void update(float dt) override;
	virtual void cleanup() override;

	void updateCurrency();
	void updateSaveInstance();

protected:
	virtual void draw(RenderTarget& target, RenderStates states) const override;

};