#pragma once

#include "Object.h"

#include <functional>

class Scene;
class Button : public Object
{
	RectangleShape* m_box;
	RectangleShape* m_overlay;
	Text* m_buttonText;
	Font* m_font;

	bool m_active = false;

	Scene* m_owner;
	std::string m_next = "none";

	bool m_bound = false;
	std::function<void()> m_func;

private:
	void initialise(Vector2f size, Color color, Color textColor, std::string text);
public:
	Button();
	Button(Vector2f size, Color color, Color textColor, std::string text = "");
	Button(float x, float y, Color color, Color textColor, std::string text = "");
	~Button();

	virtual void update(float dt) override;
	
	void setText(std::string text);
	std::string getText();
	void setCharSize(unsigned int charsize);

	void setSize(Vector2f size);
	void setSize(float x, float y);
	void setColour(Color color);

	void activate();
	void deactivate();

	void bind(std::function<void()> func);
	void unbind();
	void execute();

protected:
	virtual void draw(RenderTarget& target, RenderStates states) const override;

};