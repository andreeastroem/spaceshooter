#pragma once

#include <memory>
#include <string>
#include "ServiceLocator.h"

class Options
{
public:
	typedef std::unique_ptr<Options> ptr;
	static ptr Create();

	Options();

	std::string fontName;
	unsigned int windowWidth;
	unsigned int windowHeight;
};



