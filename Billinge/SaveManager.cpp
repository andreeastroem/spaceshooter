#include "stdafx.h"
#include "SaveManager.h"

SaveManager::ptr SaveManager::Create()
{
	return SaveManager::ptr(new SaveManager());
}

SaveManager::SaveManager()
{
	loadFile("save01");
}

SaveManager::~SaveManager()
{
	m_current = nullptr;
	for (int i = 0; i < m_saves.size(); i++)
	{
		if (m_saves[i])
		{
			delete m_saves[i];
			m_saves[i] = nullptr;
		}
	}
}

Save* SaveManager::getSaveFile()
{
	return m_current;
}

void SaveManager::loadFile(std::string filename)
{
	if (m_current)
		saveFile();

	for (int i = 0; i < m_saves.size(); i++)
	{
		if (m_saves[i]->getName() == filename)
		{
			m_current = m_saves[i];
			return;
		}
	}
	//kolla s� det blir r�tt h�r
	Save* s = new Save("../resources/spaceshooter/saves/", filename);
	if (s->load())
	{
		m_current = s;
		m_saves.push_back(s);
	}
	
}

void SaveManager::saveFile()
{
	m_current->saveSelf();
}
