#include "stdafx.h"
#include "PauseScene.h"

#include "NoHUD.h"

PauseScene::PauseScene(Scene* owner)
{
	m_owner = owner;
}

void PauseScene::update(RenderWindow* window, float dt)
{
	if (m_paused)
	{
		m_actionTimer += dt;

		if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::Y) < -30.f || Keyboard::isKeyPressed(Keyboard::Up)))
		{
			m_buttons[m_currButton]->deactivate();

			if (m_currButton == 0)
				m_currButton = m_buttons.size() - 1;
			else
				m_currButton--;

			m_buttons[m_currButton]->activate();
			m_actionTimer = 0.0f;
		}
		if (m_actionTimer > m_actionDelay && (Joystick::getAxisPosition(0, Joystick::Y) > 30.f || Keyboard::isKeyPressed(Keyboard::Down)))
		{
			m_actionTimer = 0.0f;
			m_buttons[m_currButton]->deactivate();
			m_currButton++;
			m_currButton = m_currButton % m_buttons.size();
			m_buttons[m_currButton]->activate();
			
		}

		if (m_actionTimer > m_actionDelay && (Joystick::isButtonPressed(0, 0) || Keyboard::isKeyPressed(Keyboard::Enter)))
		{
			if (m_currButton == 0)
				m_paused = false;
			else
			{
				m_buttons[m_currButton]->execute();
			}
			restart();
		}

		window->draw(*background);
		std::vector<Object*> objs = m_objectManager->getObjects();

		for each (Object* o in objs)
		{
			window->draw(*o);
		}
	}
}

void PauseScene::cleanup()
{
	__super::cleanup();
	
	if (background)
	{
		delete background;
		background = nullptr;
	}

	for (unsigned int i = 0; i < m_buttons.size(); i++)
	{
		m_buttons[i] = nullptr;
	}

	if (m_owner)
		m_owner = nullptr;
}

void PauseScene::initialise(int windowWidth, int windowHeight)
{
	__super::initialise(windowWidth, windowHeight);
	m_hud = new NoHUD;
	background = new RectangleShape(Vector2f((float)windowWidth, (float)windowHeight));
	background->setFillColor(Color(0, 0, 0, 124));

	Button* b = new Button(Vector2f(windowWidth * 0.4f, windowHeight * 0.2f), Color::Green, Color::Black, "Resume");
	b->move(windowWidth * 0.3f, windowHeight * 0.2f);
	b->activate();
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);
	
	b = new Button(Vector2f(windowWidth * 0.4f, windowHeight * 0.2f), Color::Cyan, Color::Black, "Options");
	auto changeSceneToOptions = [&]()->void
	{
		setNext("options");
	};
	b->bind(changeSceneToOptions);
	b->move(windowWidth * 0.3f, windowHeight * 0.4f);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);

	b = new Button(Vector2f(windowWidth * 0.4f, windowHeight * 0.2f), Color::Red, Color::Black, "Exit");
	auto changeSceneToStart = [&]()->void
	{
		m_owner->setNext("start");
	};
	b->bind(changeSceneToStart);
	b->move(windowWidth * 0.3f, windowHeight * 0.6f);
	m_buttons.push_back(b);
	m_objectManager->attachObject(b);

	m_paused = false;
	m_currButton = 0;

	m_actionDelay = 0.3f;
	m_actionTimer = 0.0f;

	m_name = "pause";
}

void PauseScene::setPaused(bool state)
{
	m_paused = state;
	if (m_paused)
		m_actionTimer = 0.0f;
}

bool PauseScene::isPaused()
{
	return m_paused;
}

void PauseScene::restart()
{
	m_currButton = 0;
	m_paused = false;
	m_actionTimer = 0.0f;

	for (unsigned int i = 0; i < m_buttons.size(); i++)
	{
		m_buttons[i]->deactivate();
	}
	m_buttons[0]->activate();

}
