#include "stdafx.h"
#include "Scene.h"

void Scene::draw(RenderWindow* window)
{
	window->clear();
	std::vector<Object*> objs = m_objectManager->getObjects();
	for each (Object* o in objs)
	{
		window->draw(*o);
	}
	window->draw(*m_hud);

	window->display();
}

Scene::Scene()
{

}

void Scene::initialise(int windowWidth, int windowHeight)
{
	m_windowWidth = windowWidth;
	m_windowHeight = windowHeight;
	m_objectManager = new ObjectManager();
}

void Scene::update(RenderWindow* window, float dt)
{
	m_objectManager->update(dt);
	m_hud->update(dt);

}

void Scene::restart()
{
	m_done = false;
	m_nextScene = "none";
}

void Scene::cleanup()
{
	if (m_objectManager)
	{
		delete m_objectManager;
		m_objectManager = nullptr;
	}

	if (m_hud)
	{
		m_hud->cleanup();
		delete m_hud;
		m_hud = nullptr;
	}
}

void Scene::enter()
{

}

//Consumes done status
bool Scene::isDone()
{
	bool done = m_done;
	m_done = false;
	return done;
}

std::string Scene::getNext()
{
	return m_nextScene;
}

std::string Scene::getName()
{
	return m_name;
}

void Scene::setNext(std::string next)
{
	m_done = true;
	m_nextScene = next;
}

void Scene::exit()
{
	restart();
}
