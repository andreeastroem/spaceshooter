#pragma once

#include "HUD.h"


class StartMenuHUD : public HUD
{
	sf::Font* m_font;
	sf::Text* m_logo, * m_startText;
public:
	StartMenuHUD(unsigned int windowWidth, unsigned int windowHeight);
protected:
	virtual void draw(RenderTarget& target, RenderStates states) const override;

};