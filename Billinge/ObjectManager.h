#pragma once

#include "Object.h"
#include <vector>
#include <memory>

class ObjectManager
{
	std::vector<Object*> m_objects;
	unsigned int m_value = 0;
public:

	void update(float dt);
	void attachObject(Object* object);
	std::vector<Object*> getObjects();
};