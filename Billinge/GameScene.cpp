#include "stdafx.h"
#include "GameScene.h"
#include "Asteroid.h"

#include "GameHUD.h"

#include "SaveManager.h"
#include "ServiceLocator.h"

void GameScene::update(RenderWindow* window, float dt)
{
	m_actionTimer += dt;
	//Check start button
	if ((Joystick::isButtonPressed(0, 7) || Keyboard::isKeyPressed(Keyboard::Escape)) && m_actionTimer > m_actionDelay)
	{
		m_pauseScreen->setPaused(!m_pauseScreen->isPaused());
		m_actionTimer = 0.0f;
	}
	if (!m_pauseScreen->isPaused())
	{
		m_playerCTRL->update(dt);

		m_astroidTimer += dt;
		if (m_astroidTimer > m_asteroidSpawnDelay)
		{
			Asteroid* asteroid = new Asteroid(Color(120, 120, 120, 255), m_playableArea, Vector2f(32, 32), 150, Vector2f(0, 1), 1);
			asteroid->move((float)(rand() % (m_windowWidth - 32)), m_playableArea.top);
			m_objectManager->attachObject(asteroid);
			m_astroidTimer = 0.f;
		}

		__super::update(window, dt);
		
		if (m_player->isDead())
			window->close();
	}

	window->clear(sf::Color::Black);
	std::vector<Object*> objs = m_objectManager->getObjects();
	for each (Object* o in objs)
	{
		window->draw(*o);
	}
	window->draw(*m_hud);

	if(m_pauseScreen->isPaused())
		m_pauseScreen->update(window, dt);

	window->display();
}

void GameScene::cleanup()
{
	__super::cleanup();
	
	if (m_playerCTRL)
	{
		m_playerCTRL->cleanup();
		delete m_playerCTRL;
		m_playerCTRL = nullptr;
	}

	if (m_pauseScreen)
	{
		m_pauseScreen->cleanup();
		delete m_pauseScreen;
		m_pauseScreen = nullptr;
	}
}

void GameScene::initialise(int windowWidth, int windowHeight)
{
	__super::initialise(windowWidth, windowHeight);
	
	Save* s = ServiceLocator<SaveManager>::GetService()->getSaveFile();

	m_windowWidth = windowWidth;
	m_windowHeight = windowHeight;

	m_playableArea = FloatRect(0.f, 0.f + m_windowHeight * 0.1f, (float)windowWidth, (float)m_windowHeight * 0.8f);
	m_shipSize = Vector2u(32, 64);

	m_player = new SpaceShip(Object::PLAYER);
	m_player->setSize(m_shipSize);
	m_player->setSpeed(s->getMovementSpeed());
	m_player->setAttackSpeed(s->getAttackSpeed());
	m_player->setMaxLives(s->getMaxLives());
	m_player->setColour(Color::Yellow);

	m_player->move((m_playableArea.left + m_playableArea.width) / 2,
		(m_playableArea.top + m_playableArea.height) / 2);

	m_objectManager->attachObject(m_player);

	m_asteroidSpawnDelay = 2.0f;
	m_astroidTimer = 0.f;
	m_playerCTRL = new PlayerController(m_player, m_shipSize, m_objectManager, m_playableArea);
	
	m_hud = new GameHUD(m_player, m_playableArea, m_windowWidth, m_windowHeight, 40);

	m_actionDelay = 0.3f;
	m_actionTimer = 0.0f;

	m_pauseScreen = new PauseScene(this);
	m_pauseScreen->initialise(windowWidth, windowHeight);

	m_name = "game";
}


void GameScene::enter()
{
	Save* s = ServiceLocator<SaveManager>::GetService()->getSaveFile();

	m_player->setSize(m_shipSize);
	m_player->setSpeed(s->getMovementSpeed());
	m_player->setAttackSpeed(s->getAttackSpeed());
	m_player->setMaxLives(s->getMaxLives());
	m_player->setColour(Color::Yellow);

	static_cast<GameHUD*>(m_hud)->updateSaveInstance();
}

