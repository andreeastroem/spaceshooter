#pragma once

#include "SFML/Graphics.hpp"
#include "Object.h"

class SpaceShip : public Object
{
	float m_fireTimer;
	unsigned int m_damage;

public:
	SpaceShip(TAG tag);

	void update(float dt);

	bool canFire();
	void fire();
	void damage();
	void heal(int healthToHeal);
	unsigned int getRemainingLives();

	void setSize(sf::Vector2u size);
	void setColour(sf::Color color);
	void setSpeed(float speed);
	void setAttackSpeed(float speed);
	void setMaxLives(unsigned int lives);

	virtual void collision(Object* other) override;

};